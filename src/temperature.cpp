

#include "temperature.hpp"
#include <PubSubClient.h>
#include "debug.hpp"
#include "config.hpp"

extern DallasTemperature sensors;
extern PubSubClient pubclient;

void handleTemperature(){
    redOff();
    sensors.requestTemperatures();
    float temperature = sensors.getTempCByIndex(0);
    String cTemp(temperature);
    DEBUG_PRINTF("temperature %s ºC\n", cTemp.c_str());
    pubclient.publish(MQTT_TEMP_TOPIC, cTemp.c_str());
    delay(500);
    redOn();
}
