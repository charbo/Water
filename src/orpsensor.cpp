#include "orpsensor.hpp"
#include <PubSubClient.h>

extern PubSubClient pubclient;

float orpValue;
int orpArray[ArrayLenth];
int orpArrayIndex=0;


void handleORPSensor()
{
    greenOff();
    static unsigned long orpTimer = millis(); //analog sampling interval
    //static unsigned long printTime = millis();
    bool sendResult = false;
    while (true)
    {
        if (millis() >= orpTimer)
        {
            DEBUG_PRINTF(".");
            orpTimer = millis() + 20;
            orpArray[orpArrayIndex++] = analogRead(ORPAnalogSensorPin); //read an analog value every 20ms
            if (orpArrayIndex == ArrayLenth)
            {
                orpArrayIndex = 0;
                sendResult = true;
            }

            orpValue = ((30 * (double)VOLTAGE * 1000) - (75 * avergearray(orpArray, ArrayLenth) * VOLTAGE * 1000 / 1024)) / 75 - OFFSET; //convert the analog value to orp according the circuit
        }
         
        //if (millis() >= printTime) //Every 800 milliseconds, print a numerical, convert the state of the LED indicator
        if (sendResult)
        {   
            //printTime = millis() + 800;

            String cVal((int)orpValue);
            DEBUG_PRINTF("\n");
            DEBUG_PRINTF("ORP Value : %s mV\n", cVal.c_str());
            pubclient.publish(MQTT_ORP_TOPIC, cVal.c_str());
            sendResult = false;
            break;
        }
    }
    delay(500);
    greenOn();
}
double avergearray(int *arr, int number)
{
    int i;
    int max, min;
    double avg;
    long amount = 0;
    if (number <= 0)
    {
        printf("Error number for the array to avraging!/n");
        return 0;
    }
    if (number < 5)
    { //less than 5, calculated directly statistics
        for (i = 0; i < number; i++)
        {
            amount += arr[i];
        }
        avg = amount / number;
        return avg;
    }
    else
    {
        if (arr[0] < arr[1])
        {
            min = arr[0];
            max = arr[1];
        }
        else
        {
            min = arr[1];
            max = arr[0];
        }
        for (i = 2; i < number; i++)
        {
            if (arr[i] < min)
            {
                amount += min; //arr<min
                min = arr[i];
            }
            else
            {
                if (arr[i] > max)
                {
                    amount += max; //arr>max
                    max = arr[i];
                }
                else
                {
                    amount += arr[i]; //min<=arr<=max
                }
            } //if
        }     //for
        avg = (double)amount / (number - 2);
    } //if
    return avg;
}
