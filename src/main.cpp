#include <avr/wdt.h>
#include <MemoryFree.hpp>
#include "config.hpp"
#include "utils.hpp"
#include "mqtt.hpp"
#include "wifi.hpp"
#include "temperature.hpp"
#include "orpsensor.hpp"
#include "phsensor.hpp"

/********************************* GLOBAL VARIABLE *******************************/

int wifi_status = UNKNOWN; // the Wifi radio's status
int mqtt_status = UNKNOWN;
unsigned long last_time;

bool red = false;
bool green = false;

/****************************** GLOBAL VARIABLE *******************************/
// GPIO where the DS18B20 is connected to
const int oneWireBus = TempDigitalSensorPin;

uint8_t connectToWifi();

const char *wifiToString(int type);

// Initialize the Ethernet client object
WiFiClient client;
PubSubClient pubclient(client);
OneWire oneWire(oneWireBus);
DallasTemperature sensors(&oneWire);

void setup()
{

  pinMode(RED_LED, OUTPUT);
  pinMode(GREEN_LED, OUTPUT);
  greenOff();
  redOff();

  // initialize serial for debugging
  Serial.begin(115200);
  sensors.begin();
  // initialize serial for ESP module
  Serial1.begin(115200);
  // initialize ESP module
  Serial1.resetESP();
  Serial1.overflow(); // clear the flag. after reset in init esp overflows buffer with boot log

  if (!WiFi.init(&Serial1))
    die("<ERROR> could not init WIFIi\n");

  //connect to MQTT server
  pubclient.setServer("lhotse.nextmind.net", 1883);
  pubclient.setCallback(callback);

  last_time = millis();
  //init sensors

  DEBUG_PRINTLN("Init done ");
} //end setup
void handleFreeMemory()
{
  String cVal(freeMemory());
  DEBUG_PRINTF("Memory Value : %s \n", cVal.c_str());
  pubclient.publish(MQTT_MEMORY_TOPIC, cVal.c_str());
}
void redOn()
{
  if (!red)
  {
    digitalWrite(RED_LED, HIGH);

  
    red = true;
  }
}
void redOff()
{
  if (red)
  {
    digitalWrite(RED_LED, LOW);

    red = false;
  }
}
void greenOff()
{
  if (green)
  {
    digitalWrite(GREEN_LED, LOW);

    green = false;
  }
}
void greenOn()
{
  if (!green)
  {
    digitalWrite(GREEN_LED, HIGH);

    green = true;
  }
}

void loop()
{

  handleWifi();
  handleMQTT();

  unsigned long time = millis();
  if (time - last_time > TIME_OUT_CHECK && mqtt_status == MQTT_CONNECTED && wifi_status == WL_CONNECTED)
  {
    wdt_enable(WDTO_8S);
    handleTemperature();
    handleORPSensor();
    handlePHSensor();
    handleFreeMemory();
    last_time = time;
  }

  wdt_reset();
  redOff();
  greenOff();
  delay(SLEEP_TIME);
  greenOn();
  redOn();
}
