#ifndef MQTT_HANDLER_H
#define MQTT_HANDLER_H
#include <UnoWiFiDevEdSerial1.h>
#include <WiFiEspAT.h>
#include <PubSubClient.h>
#include "utils.hpp"



uint8_t connectToMQTT();
void callback(char *topic, byte *payload, unsigned int length);
void handleMQTT();
uint8_t connectToMQTT();

#endif