#include "debug.hpp"
#if defined (_DEBUG_) || defined (_TRACE_) || defined (_INFO_)
void pex(const char *fmt, ... ){
        char buf[256]; // resulting string limited to 128 chars
        va_list args;
        va_start (args, fmt );
        vsnprintf(buf, 128, fmt, args);
        va_end (args);
        Serial.print(buf);
}
#else
void pex(const char *fmt, ... ){
}

#endif

