#ifndef __DEBUG_H__
#define __DEBUG_H__
#include <Arduino.h>
#include <stdarg.h>
void pex(const char *fmt, ...);

#ifdef _DEBUG_
#define DEBUG_PRINTF(format, ...) pex(format, ##__VA_ARGS__)
#define DEBUG_PRINTLN(x) Serial.println(x);
#define DEBUG_PRINT(x) Serial.print(x);
#else
#define DEBUG_PRINTF(format, ...)
#define DEBUG_PRINTLN(x)
#define DEBUG_PRINT(x)
#endif
#ifdef _TRACE_
#define TRACE_PRINTF(format, ...) pex(format, ##__VA_ARGS__)
#define TRACE_PRINTLN(xx) Serial.println(xx);
#define TRACE_PRINT(xx) Serial.print(xx);
#else
#define TRACE_PRINTF(format, ...)
#define TRACE_PRINTLN(x)
#define TRACE_PRINT(xx) ;
#endif
#ifdef _INFO_

#define INFO_PRINTF(format, ...) pex(format, ##__VA_ARGS__)
#define INFO_PRINT(xx) Serial.print(xx);
#define INFO_PRINTLN(xx) Serial.println(xx);
#else
#define INFO_PRINTF(format, ...)
#define INFO_PRINT(xx)
#define INFO_PRINTLN(xx)

#endif
#endif