// memoryFree header
// From http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1213583720/15
// ...written by user "mem".

#ifndef MEMORY_FREE_H
#define MEMORY_FREE_H
int freeMemory();

#ifdef _LEAKS_
static int free_begin = 0;
static int free_end = 0;
static int delta = 0;

#define BEGIN_LEAK free_begin = freeMemory();

#define END_LEAK  free_end = freeMemory(); delta = free_begin - free_end; if (delta == 0) DEBUG_PRINT("--No LEAKS -- \n"); else  DEBUG_PRINT("---> MEMORY (%d) End(%d)<---  delta [  %d  ]\n", free_begin, free_end, delta);
#else
#define BEGIN_LEAK free_begin 

#define END_LEAK  

#endif

#endif
