#include "utils.hpp"
#include <UnoWiFiDevEdSerial1.h>
#include <WiFiEspAT.h>
#include <PubSubClient.h>
const char *wifiToString(int type)
{
#ifdef TOSTRING
  switch (type)
  {
  case WL_CONNECTED:
    return "WL_CONNECTED";
  case WL_CONNECTION_LOST:
    return "WL_CONNECTION_LOST";
  case WL_AP_FAILED:
    return "WL_AP_FAILED";
  case WL_DISCONNECTED:
    return "WL_DISCONNECTED";
  case WL_NO_SHIELD:
    return "WL_NO_SHIELD";
  case WL_IDLE_STATUS:
    return "WL_IDLE_STATUS";
  case WL_CONNECT_FAILED:
    return "WL_CONNECT_FAILED";
  case WL_AP_LISTENING:
    return "WL_AP_LISTENING";
  case WL_AP_CONNECTED:
    return "WL_AP_CONNECTED";
  }
  return "WL_UNKNOW";
#else
  char *b = new char[5];
  itoa(type, b, 10);
  return b;
#endif
}
const char *mqttToString(int type)
{
#ifdef TOSTRING

  switch (type)
  {
  case MQTT_CONNECTION_TIMEOUT:
    return "MQTT_CONNECTION_TIMEOUT";
  case MQTT_CONNECTION_LOST:
    return "MQTT_CONNECTION_LOST";
  case MQTT_CONNECT_FAILED:
    return "MQTT_CONNECT_FAILED";
  case MQTT_DISCONNECTED:
    return "MQTT_DISCONNECTED";
  case MQTT_CONNECTED:
    return "MQTT_CONNECTED";
  case MQTT_CONNECT_BAD_PROTOCOL:
    return "MQTT_CONNECT_BAD_PROTOCOL";
  case MQTT_CONNECT_BAD_CLIENT_ID:
    return "MQTT_CONNECT_BAD_CLIENT_ID";
  case MQTT_CONNECT_UNAVAILABLE:
    return "MQTT_CONNECT_UNAVAILABLE";
  case MQTT_CONNECT_BAD_CREDENTIALS:
    return "MQTT_CONNECT_BAD_CREDENTIALS";
  case MQTT_CONNECT_UNAUTHORIZED:
    return "MQTT_CONNECT_UNAUTHORIZED";
  }

  return "MQTT_UNKNOW";
#else
  char *b = new char[5];
  itoa(type, b, 10);
  return b;
#endif
}
#ifdef _DEBUG_
void die(String msg)
{
  INFO_PRINT(msg.c_str());
  while (true)
    ;
}
#else
void die(String msg){}
#endif

