#include "wifi.hpp"
#include "config.hpp"


extern int wifi_status;
const char ssid[] = _SSID_;
const char pass[] = _PASS_;
extern  bool red ;
extern bool green ;
uint8_t connectToWifi()
{

  uint8_t curr_status = WL_DISCONNECTED;
  // attempt to connect to WiFi network
  DEBUG_PRINTF("[ WIFI ]--> Attempting to connect to WPA SSID: %s ", ssid);
  int i = 0;
  while (curr_status != WL_CONNECTED)
  {
    if((i%2)==0)
      greenOn();
    else
      redOn();
    curr_status = WiFi.begin(ssid, pass);
    DEBUG_PRINT(".");
    if((i%2)==0)
      greenOff();
    else
      redOff();
    i++;
  }
  DEBUG_PRINTLN(" connected \n");
  greenOn();
  printWifiStatus();
  return curr_status;
}

void printWifiStatus()
{
#ifdef _TRACE_

  TRACE_PRINT("[ WIFI ]--> SSID:");
  TRACE_PRINT(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  TRACE_PRINT("  IP Address: ");
  TRACE_PRINT(ip);

  // print the received signal strength
  long rssi = WiFi.RSSI();
  TRACE_PRINT("  Signal strength (RSSI):");
  TRACE_PRINT(rssi);
  TRACE_PRINTLN(" dBm");
#endif // TRACE
}

void handleWifi()
{
  int current_status;
  if (wifi_status != UNKNOWN)
    current_status = WiFi.status();
  switch (wifi_status)
  {
  case WL_CONNECTED:
    greenOn();
    break;
  case WL_CONNECTION_LOST:
    
    wifi_status = WiFi.disconnect();
    break;
  case WL_AP_FAILED:
    break;
  case UNKNOWN:
  case WL_DISCONNECTED:
    greenOff();
    current_status = connectToWifi();
    break;
  case WL_NO_SHIELD:
  case WL_IDLE_STATUS:
  case WL_CONNECT_FAILED:
  case WL_AP_LISTENING:
  case WL_AP_CONNECTED:
    greenOff();
    break;
  }
  if (current_status != wifi_status)
  {
    DEBUG_PRINTF("[ WIFI ]--> status changed from  [%s]-->%s\n", wifiToString(wifi_status), wifiToString(current_status));
    wifi_status = current_status;
  }
}
