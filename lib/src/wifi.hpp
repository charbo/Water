#ifndef WIFI_HANDLER_H
#define WIFI_HANDLER_H
#include <UnoWiFiDevEdSerial1.h>
#include <WiFiEspAT.h>
#include <PubSubClient.h>
#include "utils.hpp"
#include "debug.hpp"




uint8_t connectToWifi();
void printWifiStatus();
void handleWifi();



#endif