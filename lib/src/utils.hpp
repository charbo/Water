#ifndef UTILS_HANDLER_H
#define UTILS_HANDLER_H
#include <Arduino.h>
#include "debug.hpp"
const char *wifiToString(int type);
const char *mqttToString(int type);
void die(String msg);

#endif
