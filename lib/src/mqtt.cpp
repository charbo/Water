#include "mqtt.hpp"
#include "config.hpp"

extern int mqtt_status;
extern PubSubClient pubclient;
extern int wifi_status;
extern WiFiClient client;
const char mqtt_user[] = MQTT_USER;
const char mqtt_pass[] = MQTT_PASS;
const char mqtt_in_topic[] = MQTT_IN_TOPIC;
const char mqtt_out_topic[] = MQTT_IN_TOPIC;

void waitForResponse(int max)
{

  for (int i = 0; i < max; i++)
  {
    while (Serial1.available())
    {
      String inData = Serial1.readStringUntil('\n');
      if (inData.length() > 1)
        INFO_PRINT("[ MQTT ]--> Got reponse from ESP8266:  ");
      INFO_PRINTLN(inData.c_str());
      i = max;
    }
    delay(30);
  }
}
void handleATCommands(String msg)
{
  if (msg.startsWith("AT"))
    Serial1.println(msg.c_str());
}
//print any message received for subscribed topic
void callback(char *topic, byte *payload, unsigned int length)
{

  String msg;
  for (unsigned int i = 0; i < length; i++)
  {
    char receivedChar = (char)payload[i];
    msg += receivedChar;
  }

  INFO_PRINTF("[ MQTT ]--> reveived msg: [ %s ]\n", msg.c_str());
  //delay(20);
  handleATCommands(msg);
  waitForResponse(300);
}

void handleMQTT()
{
  if (WL_CONNECTED != wifi_status)
    return;

  int current_status = UNKNOWN;
  if (mqtt_status != UNKNOWN)
    current_status = pubclient.state();
  switch (current_status)
  {
  case MQTT_CONNECTION_TIMEOUT:
  case MQTT_CONNECTION_LOST:
  case MQTT_CONNECT_FAILED:
  case MQTT_CONNECT_BAD_PROTOCOL:
  case MQTT_CONNECT_BAD_CLIENT_ID:
  case MQTT_CONNECT_UNAVAILABLE:
  case MQTT_CONNECT_BAD_CREDENTIALS:
  case MQTT_CONNECT_UNAUTHORIZED:
  case MQTT_DISCONNECTED:
    pubclient.disconnect();
  case UNKNOWN:
    current_status = connectToMQTT();
    redOff();
  case MQTT_CONNECTED:
    redOn();
    if (!pubclient.loop())
      current_status = pubclient.state();
    break;
  }
  if (current_status != mqtt_status)
  {
    INFO_PRINTF("[ MQTT ]--> status changed from  [%s]-->%s\n", mqttToString(mqtt_status), mqttToString(current_status));
    mqtt_status = current_status;
  }
}

uint8_t connectToMQTT()
{

  DEBUG_PRINTF("[ MQTT ]--> Attempting MQTT connection .");
  int max_retry = MQTT_MAX_RETRY;
  ///MQTT STUFFS
  IPAddress ip = WiFi.localIP();
  char *buf = (char *)malloc(128);
  sprintf(buf, "%u.%u.%u.%u", ip[0], ip[1], ip[2], ip[3]);

  DEBUG_PRINTLN(buf);

  while (!pubclient.connect(buf, mqtt_user, mqtt_pass) && max_retry > 0)
  {
    DEBUG_PRINTF(".");
    redOn();
    delay(1000);
    redOff();
    max_retry--;
  }
#ifdef _DEBUG_
  if (max_retry != MQTT_MAX_RETRY)
    DEBUG_PRINTF("\n");
#endif // DEBUG
  TRACE_PRINTF("[ MQTT ]--> connected to server --> subscribed to topic [ %s ] ", mqtt_in_topic);
  if (pubclient.subscribe(mqtt_in_topic, 0))
    TRACE_PRINTF(" Successfully  \n");
  else
    TRACE_PRINTF(" fails :(  \n");

  return pubclient.state();
}